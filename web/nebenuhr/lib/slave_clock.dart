import 'dart:async';
import 'dart:convert' as convert;
import 'rpc.dart';

const int _timedActionsCount = 10;
const int _turnOnAction = 1;
const int _turnOffAction = 0;
const int _doNothingAction = 99;
const _actionToStr = const {
  _turnOnAction: 'turn on',
  _turnOffAction: 'turn off',
  _doNothingAction: 'do nothing'
};

const _nToWeekday = const {
  0: 'Sunday',
  1: 'Monday',
  2: 'Tuesday',
  3: 'Wednesday',
  4: 'Thursday',
  5: 'Friday',
  6: 'Saturday',
  7: 'Everyday',
  8: 'Monday - Friday',
  9: 'Saturday - Sunday'
};

Future<int> _asInt(Future<String> sF) async {
  var s = await sF;
  return int.parse(s);
}

Future<int> _getDisplayTime() async => _asInt(rpcApp('Public.DisplayTime'));

Future _setDisplayTime(int newDisplayTime) =>
    rpcApp('Public.DisplayTime', {'displayTime': newDisplayTime});

Future<int> _getEpochTime() => _asInt(rpcApp('Public.EpochTime'));

Future _setEpochTime(int newEpochTime) =>
    rpcApp('Public.EpochTime', {'epochTime': newEpochTime});

Future<int> _getDisplayState() => _asInt(rpcApp('Public.DisplayState'));

Future _setDisplayState(int newDisplayState) =>
    rpcApp('Public.DisplayState', {'displayState': newDisplayState});

Future<int> _getSummertimeSwitch() => _asInt(rpcApp('Public.SummertimeSwitch'));

Future _setSummertimeSwitch(int newSummertimeSwitch) => rpcApp(
    'Public.SummertimeSwitch', {'summertimeSwitch': newSummertimeSwitch});

Future<_TimedAction> _getTimedAction(int nb) async {
  var timedActionJson =
      await rpcApp('Public.TimedAction', {'nb': nb, 'readWrite': 'r'});
  var timedActionValues = convert.json.decode(timedActionJson);
  _TimedAction res = new _TimedAction();
  res.at = timedActionValues['at'];
  res.action = timedActionValues['action'];
  res.weekday = timedActionValues['weekday'];
  return res;
}

Future _setTimedAction(int nb, _TimedAction timedAction) =>
    rpcApp('Public.TimedAction', {
      'nb': nb,
      'at': timedAction.at,
      'weekday': timedAction.weekday,
      'action': timedAction.action
    });

void _updateDisplayState(int state) {
  var s = 'unknown';
  switch (state) {
    case 0:
      s = 'not paused';
      break;
    case 1:
      s = 'manually paused';
      break;
    case 2:
      s = 'automatically paused';
      break;
  }
  q('#displayState').text = s;
  qi('#pausedTargetValue').value = (state == 0) ? '1' : '0';
  qb('#pausedButton').text = state == 0 ? "pause" : "unpause";
}

void _updateSummertimeSwitch() async {
  var sts = await _getSummertimeSwitch();
  var s = 'unknown';
  switch (sts) {
    case 0:
      s = 'automatic';
      break;
    case 1:
      s = 'always summertime';
      break;
    case 2:
      s = 'always wintertime';
      break;
  }
  q('#summertimeSwitch').text = s;
  qs('#summertimeSwitchSelector').value = '0';
}

Future _updateDisplayPaused() async {
  var displayState = await _getDisplayState();
  _updateDisplayState(displayState);
}

Future _updateEpochTime() async {
  var epochTime = await _getEpochTime();
  var nebenUhrTime = new DateTime.fromMillisecondsSinceEpoch(epochTime * 1000);
  q('#epochTime').text = '$nebenUhrTime';
  q('#currentTime').text = '${new DateTime.now()}';
}

Future _updateDisplayTime() async {
  var displayTime = await _getDisplayTime();
  if (displayTime == 0xFFFF) {
    q('#displayTime').text = 'UNKNOWN';
  } else {
    var hours = displayTime ~/ 60;
    var minutes = displayTime % 60;
    q('#displayTime').text = '$hours : $minutes';
  }
}

class _TimedAction {
  int at;
  int action;
  int weekday;
}

Future _updateTimedActions() async {
  String toTime(int t) => '${t ~/ 60}:${t % 60}';
  String toAction(int action) => _actionToStr[action];
  String toWeekday(int weekday) => _nToWeekday[weekday];

  var table = qtable('#timedActions');
  while (table.rows.isNotEmpty) table.deleteRow(0);
  for (int i = 0; i < _timedActionsCount; i++) {
    var timedAction = await _getTimedAction(i);
    var newRow = table.addRow();
    newRow.addCell().text = '$i';
    newRow.addCell().text = '${toWeekday(timedAction.weekday)}';
    newRow.addCell().text = '${toTime(timedAction.at)}';
    newRow.addCell().text = '${toAction(timedAction.action)}';
  }
}

Future _update() async {
  await _updateDisplayPaused();
  await _updateEpochTime();
  await _updateDisplayTime();
  await _updateTimedActions();
  await _updateSummertimeSwitch();
}

int _getInt(String inputId) {
  String v = qn('$inputId').value;
  return int.tryParse(v ?? '0') ?? 0;
}

int _getSelInt(String id) {
  String v = qs('$id').value;
  return int.tryParse(v ?? '0') ?? 0;
}

Future initialize() async {
  registerClickHandler(
      '#epochTimeButton',
      () => _setEpochTime((new DateTime.now().millisecondsSinceEpoch) ~/ 1000),
      _updateEpochTime);

  registerClickHandler(
      '#displayTimeButton',
      () => _setDisplayTime(
          (_getInt('#displayHour') % 12) * 60 + _getInt('#displayMinutes')),
      _updateDisplayTime);

  registerClickHandler(
      '#pausedButton',
      () => _setDisplayState(_getInt('#pausedTargetValue')),
      _updateDisplayPaused);

  registerClickHandler(
      '#setSummertimeSwitchButton',
      () => _setSummertimeSwitch(_getSelInt('#summertimeSwitchSelector')),
      _updateSummertimeSwitch);

  registerClickHandler('#setTimedActionButton', () {
    var ta = new _TimedAction();
    ta.at = _getInt('#timedActionHour') * 60 + _getInt('#timedActionMinutes');
    ta.action = _getSelInt('#timedActionAction');
    ta.weekday = _getSelInt('#timedActionWeekday');
    return _setTimedAction(_getInt('#timedActionNb'), ta);
  }, _updateTimedActions);

  await _update();
  new Timer.periodic(new Duration(seconds: 30), (Timer t) => _update());
}
