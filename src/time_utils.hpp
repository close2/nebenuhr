#pragma once

#include <stdint.h>

namespace timeUtils {
    
  enum class SummertimeSwitch:uint8_t {
    AUTOMATIC = 0,
    ALWAYS_SUMMER = 1,
    ALWAYS_WINTER = 2
  };


  // http://forum.arduino.cc/index.php?topic=154768.msg1163549#msg1163549
  // Input time must be GMT
  static bool isSummerTime(uint16_t year, uint8_t month, uint8_t day, uint8_t hour) {
    // European Daylight Savings Time calculation by "jurs" for German Arduino Forum
    // input parameters: "normal time" for year, month, day, hour.
    // return value: returns true during Daylight Saving Time, false otherwise
    if (month < 3 || month > 10) return false; // keine Sommerzeit in Jan, Feb, Nov, Dez
    if (month > 3 && month < 10) return true; // Sommerzeit in Apr, Mai, Jun, Jul, Aug, Sep
    return month == 3 && (hour + 24 * day) >= (1 + 24 * (31 - (5 * year / 4 + 4) % 7)) ||
           month == 10 && (hour + 24 * day) < (1 + 24 * (31 - (5 * year / 4 + 1) % 7));
  }

  typedef struct tm {
    uint8_t  tm_sec;         /* seconds,  range 0 to 59                       */
    uint8_t  tm_min;         /* minutes, range 0 to 59                        */
    uint8_t  tm_hour;        /* hours, range 0 to 23                          */
    uint8_t  tm_mday;        /* day of the month, range 1 to 31               */
    uint8_t  tm_mon;         /* month, range 0 to 11                          */
    uint16_t tm_year;        /* The number of years since 1900                */
    uint8_t  tm_wday;        /* day of the week, range 0 to 6  (0 == sunday)  */
    uint16_t tm_yday;        /* day in the year, range 0 to 365               */
    bool     tm_isdst;       /* daylight saving time                          */
  } tm_t;

  const uint32_t SECS_DAY = 24 * 60 * 60;
  const uint16_t EPOCH_YR = 1970;
  const uint16_t YEAR0 = 1900;
  constexpr bool LEAPYEAR(uint16_t year) { return (!((year) % 4) && (((year) % 100) || !((year) % 400))); }
  constexpr uint16_t YEARSIZE(uint16_t year) { return (LEAPYEAR(year) ? 366 : 365); }

  // https://stackoverflow.com/questions/1692184/converting-epoch-time-to-real-date-time/35199669
  static tm_t epochToTimeDate(const time_t& time) {
    tm_t timep;
    uint16_t year = EPOCH_YR;

    uint32_t dayclock = time % SECS_DAY;
    uint16_t dayno = time / SECS_DAY;
    uint8_t _ytab[2][12] = {{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
                            {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}};

    timep.tm_sec = dayclock % 60;
    timep.tm_min = (dayclock % 3600) / 60;
    timep.tm_hour = dayclock / 3600;
    timep.tm_wday = (dayno + 4) % 7;       /* day 0 was a thursday */
    while (dayno >= YEARSIZE(year)) {
      dayno -= YEARSIZE(year);
      year++;
    }
    timep.tm_year = year - YEAR0;
    timep.tm_yday = dayno;
    timep.tm_mon = 0;
    while (dayno >= _ytab[LEAPYEAR(year) ? 1 : 0][timep.tm_mon]) {
      dayno -= _ytab[LEAPYEAR(year) ? 1 : 0][timep.tm_mon];
      timep.tm_mon++;
    }
    timep.tm_mday = dayno + 1;
    timep.tm_isdst = false;

    return timep;
  }

  static tm_t epochToMez(const time_t& epoch, SummertimeSwitch summertimeSwitch) {
    tm_t timeDate = epochToTimeDate(epoch);
    bool isMezSummerTime = summertimeSwitch == SummertimeSwitch::AUTOMATIC ?
      isSummerTime(timeDate.tm_year + 1900, timeDate.tm_mon + 1, timeDate.tm_mday, timeDate.tm_hour) :
      summertimeSwitch == SummertimeSwitch::ALWAYS_SUMMER;
    if (isMezSummerTime) {
      if (timeDate.tm_hour < 22) timeDate.tm_hour += 2;
      else timeDate = epochToTimeDate(epoch + 2 * 60 * 60);
      timeDate.tm_isdst = true;
    } else {
      if (timeDate.tm_hour < 23) timeDate.tm_hour += 1;
      else timeDate = epochToTimeDate(epoch + 1 * 60 * 60);
    }
    return timeDate;
  }
}
