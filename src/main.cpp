#include "mgos.h"
//#include "mgos_mqtt.h"
#include "mgos_updater_common.h"

#include "slave_clock.hpp"



// static void button_cb(int pin, void *arg) {
//   char topic[100], message[100];
//   struct json_out out = JSON_OUT_BUF(message, sizeof(message));
//   snprintf(topic, sizeof(topic), "/devices/%s/events",
//            mgos_sys_config_get_device_id());
//   json_printf(&out, "{total_ram: %lu, free_ram: %lu}",
//               (unsigned long) mgos_get_heap_size(),
//               (unsigned long) mgos_get_free_heap_size());
//   bool res = mgos_mqtt_pub(topic, message, strlen(message), 1, false);
//   LOG(LL_INFO, ("Pin: %d, published: %s", pin, res ? "yes" : "no"));
//   (void) arg;
// }

enum mgos_app_init_result mgos_app_init(void) {
  /* Publish to MQTT on button press */
  // mgos_gpio_set_button_handler(mgos_sys_config_get_pins_button(),
  //                              MGOS_GPIO_PULL_UP, MGOS_GPIO_INT_EDGE_NEG, 20,
  //                              button_cb, NULL);
 

  bool doFactoryReset = mgos_sys_config_get_device_do_factory_reset();
  if (doFactoryReset) mgos_sys_config_set_device_do_factory_reset(false);

  auto resSlaveClock = slaveClock::slaveClock_init(doFactoryReset);

  if (resSlaveClock == MGOS_INIT_OK) {
    mgos_upd_commit();
  }

  return MGOS_APP_INIT_SUCCESS;
}
