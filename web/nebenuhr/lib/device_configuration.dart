import 'dart:async';
import 'dart:convert' as convert;
import 'dart:html' as html;
import 'rpc.dart';

String _adminPassword = '';

Future<String> _rpcAdmin(String path,
        [Map<String, dynamic> rpcArgs = const {}]) =>
    rpcAdmin(path, _adminPassword, rpcArgs);

Future<String> _getWifiSsid() async => _rpcAdmin('Config.Get')
    .then((json) => convert.json.decode(json)['wifi']['sta']['ssid']);

Future _setWifiCredentials(String ssid, String wifiPassword) {
  var rpcArgs = {
    'config': {
      'wifi': {
        'ap': {'enable': false},
        'sta': {'enable': true, 'ssid': ssid, 'pass': wifiPassword}
      }
    }
  };
  return _rpcAdmin('Config.Set', rpcArgs);
}

Future _saveAndReboot() {
  q('#main').classes.add('hide');
  q('#rebootMsg').classes.remove('hide');
  return _rpcAdmin('Config.Save', {'reboot': true});
}

Future _updateWifiSsid() async {
  var ssid = await _getWifiSsid();
  q('#ssid').text = ssid;
}

Future _update() async {
  if (_adminPassword != '') return _updateWifiSsid();
}

Future initialize() async {
  qb('#adminPwButton').onClick.listen((_) {
    _adminPassword = qp('#adminPassword').value;
    _rpcAdmin('Config.Get').catchError((e) {
      html.window.alert(e);
      _adminPassword = '';
      return null;
    }).then((c) {
      if (c != null) {
        print(c);
        _update();
        q('#adminOperations').classes.remove('hide');
      }
    });
  });

  registerClickHandler('#wifiDataButton', () {
    var newSsid = qt('#newSsid').value;
    var newPassword = qp('#newWifiPassword').value;
    return _setWifiCredentials(newSsid, newPassword);
  }, _updateWifiSsid);

  registerClickHandler('#saveAndRebootButton', _saveAndReboot, () {});

  await _update();
  new Timer.periodic(new Duration(seconds: 30), (Timer t) => _update());
}
