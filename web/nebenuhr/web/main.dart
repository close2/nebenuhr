import 'dart:html';
import 'package:nebenuhr/device_configuration.dart' as adminArea;
import 'package:nebenuhr/slave_clock.dart' as slaveClockArea;

main() async {
  querySelector('#loading').classes.add('hide');
  querySelector('#main').classes.remove('hide');
  await slaveClockArea.initialize();
  await adminArea.initialize();
}
