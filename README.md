# Nebenuhr  (german for slave-clock)

## Overview

This project controls a slave-clock.

The code retrieves the current time using sntp.  There are 2 output
pins.  Every minute one of those pins (alternating) is set high for
a short period of time.

A H-bridge will then deliver 18V to the slave-clock motor.


## Mongoose-OS

This project uses mongoose-os which is apache licensed.
I do however want to include OTA functionality which is only GPL
(or closed source).

This is the reason this project is GPL as well.

I have also based some code on https://github.com/mongoose-os-apps/wifi-setup-web (apache license)

If you want another license, contact me.

## How to install this app

- Install and start [mos tool](https://mongoose-os.com/software.html)
- Switch to the Project page, find and import this app, build and flash it:

<p align="center">
  <img src="https://mongoose-os.com/images/app1.gif" width="75%">
</p>

## PCB

See my other repository for the hardware part.

