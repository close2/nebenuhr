import 'dart:async';
import 'dart:convert' as convert;
import 'dart:html';

const String user = 'app';
const String password = 'app';

String adminUser = 'admin';

Future<String> rpcApp(String path, [Map<String, dynamic> rpcArgs = const {}]) {
  rpcArgs = new Map.from(rpcArgs);
  if (!rpcArgs.containsKey('readWrite')) {
    rpcArgs['readWrite'] = rpcArgs.isEmpty ? 'r' : 'w';
  }
  var req = new HttpRequest();
  req.open('POST', '/rpc/$path', async: true, user: user, password: password);
  req.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
  req.send(convert.json.encode(rpcArgs));
  return req.onLoad.first.then((pV) => req.responseText);
}

Future<String> rpcAdmin(String path, String adminPassword,
    [Map<String, dynamic> rpcArgs = const {}]) {
  var req = new HttpRequest();
  req.open('POST', '/rpc/$path',
      async: true, user: adminUser, password: adminPassword);
  req.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
  req.send(convert.json.encode(rpcArgs));
  return req.onLoad.first.then((pV) => req.responseText);
}

Element q(String selector) => querySelector(selector);

SelectElement qs(String id) => querySelector(id) as SelectElement;
TextInputElement qt(String id) => querySelector(id) as TextInputElement;
NumberInputElement qn(String id) => querySelector(id) as NumberInputElement;
ButtonElement qb(String id) => querySelector(id) as ButtonElement;
InputElement qi(String id) => querySelector(id) as InputElement;
PasswordInputElement qp(String id) => querySelector(id) as PasswordInputElement;
TableElement qtable(String id) => querySelector(id) as TableElement;

void registerClickHandler(String id, var function, var updateF) {
  var el = q(id);
  if (el == null) print("el $id could not be found");
  else el.onClick.listen((e) => function().then((_) => updateF()));
}
