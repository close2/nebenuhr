#pragma once

#include "mgos.h"
#include "mgos_rpc.h"
#include "mgos_vfs.h"

#include <inttypes.h>

#include "time_utils.hpp"

#undef PRId32
#define PRId32 "ld"

#undef PRIu32
#define PRIu32 "u"

#undef SCNu32
#define SCNu32 "ul"

namespace slaveClock {

  // june 5 2018 (the time I was programming this)
  const int32_t _minValidEpochTime = 1528194675;

  const char* _timedActionFileName = "ta.bin";

  enum class _Action:uint8_t {
    TURN_DISPLAY_OFF = 0,
    TURN_DISPLAY_ON = 1,
    DO_NOTHING = 99
  };

  enum class _DisplayState:uint8_t {
    ON = 0,
    MAN_PAUSED = 1,
    AUT_PAUSED = 2
  };

  const char* _displayStateStr[3] = { "On", "man. paused", "aut. paused"};

  enum class _Weekday:uint8_t {
    SU = 0,
    MO = 1,
    TU = 2,
    WE = 3,
    TH = 4,
    FR = 5,
    SA = 6,
    ALL = 7,
    MO_FR = 8,
    SA_SU = 9
  };

  typedef class _TimedAction {
  public:
    _TimedAction() :
      at(0),
      action(_Action::DO_NOTHING),
      weekday(_Weekday::ALL) {}

    uint16_t at;
    enum _Action action;
    enum _Weekday weekday;
  } _timedAction_t;

  const uint16_t _unknownDisplayTime = 0xFFFF;
  const uint8_t _timedActionsCount = 10;

  typedef class ClockData {
  public:
    ClockData() :
      displayTime(_unknownDisplayTime),
      realTime(0),
      epochTime(0),
      tmpEpochTimeOffset(0),
      displayState(_DisplayState::ON),
      hBridgeIsActive(false),
      buttonPressLedTmr(MGOS_INVALID_TIMER_ID) {}

    // Minutes since midnight.  Time which is currently shown on the clock.
    // 0 <= displayTime < 12 * 60
    uint16_t displayTime = _unknownDisplayTime;

    // Minutes since midnight, the real time.
    // If !displayPaused this is the target.
    // 0 <= realTime < 12 * 60
    uint16_t realTime = 0;

    enum timeUtils::SummertimeSwitch summertimeSwitch = timeUtils::SummertimeSwitch::AUTOMATIC;

    uint32_t epochTime = 0;

    // This time is added before calculating realTime from epochTime.
    // It will be cleared when sntp sets a correct time.
    // While the system clock is too low we will add this value
    // to the system clock for the time calculation.
    uint32_t tmpEpochTimeOffset = 0;

    enum _DisplayState displayState = _DisplayState::ON;

    mgos_timer_id outputCbTimerId;

    bool hBridgeIsActive = false;

    mgos_timer_id buttonPressLedTmr = MGOS_INVALID_TIMER_ID;

    _timedAction_t actions[_timedActionsCount];
  } _clockData_t;



  static void saveUserData(_clockData_t& clockData) {
    auto fd = mgos_vfs_open(_timedActionFileName, O_CREAT | O_TRUNC | O_WRONLY, 0);
    if (fd < 0) {
      LOG(LL_ERROR, ("Could not open user data file %s for saving.", _timedActionFileName));
      return;
    }

    ssize_t bytesWritten = 0;
    uint8_t* src = (uint8_t*) clockData.actions;
    auto len = sizeof(clockData.actions);
    while (auto res = mgos_vfs_write(fd, src + bytesWritten, len - bytesWritten) >= 0 &&
           (len - bytesWritten) > 0) bytesWritten += res;

    mgos_vfs_close(fd);
  }

  static void removeUserData() {
    // Delete timedAction file.
    mgos_vfs_unlink(_timedActionFileName);
  }

  static void readUserData(_clockData_t& clockData) {
    auto fd = mgos_vfs_open(_timedActionFileName, O_RDONLY, 0);
    if (fd < 0) {
      LOG(LL_ERROR, ("Could not open user data file %s.", _timedActionFileName));
      return;
    }

    ssize_t bytesRead = 0;
    uint8_t* dst = (uint8_t*) clockData.actions;
    auto len = sizeof(clockData.actions);

    while (auto res = mgos_vfs_read(fd, dst + bytesRead, len - bytesRead) >= 0 &&
           (len - bytesRead) > 0) bytesRead += res;

    mgos_vfs_close(fd);
  }


  static void _buttonPressLedOffCb(void *arg) {
    _clockData_t& clockData = *((_clockData_t*)arg);
    if (clockData.buttonPressLedTmr != MGOS_INVALID_TIMER_ID) {
      // This is possibly the timer, which called this function.
      mgos_clear_timer(clockData.buttonPressLedTmr);
      clockData.buttonPressLedTmr = MGOS_INVALID_TIMER_ID;
    }
    LOG(LL_DEBUG, ("Turning pins off."));
    mgos_gpio_write(mgos_sys_config_get_pins_led_green(), false);
    mgos_gpio_write(mgos_sys_config_get_pins_led_red(), false);
  }

  static bool displayPaused(const _clockData_t& clockData) {
    return clockData.displayState != _DisplayState::ON;
  }

  static void setDisplayPaused(enum _DisplayState newDisplayState, _clockData_t& clockData) {
    LOG(LL_DEBUG, ("setDisplayPaused.  Currently display is in state %s.  Will set to %s.",
                _displayStateStr[(uint8_t) clockData.displayState],
                _displayStateStr[(uint8_t) newDisplayState]));

    clockData.displayState = newDisplayState;

    // If paused is called during the led on time, we abort the currently
    // displayed led.
    _buttonPressLedOffCb(&clockData);

    auto pinGreenLed = mgos_sys_config_get_pins_led_green();
    auto pinRedLed = mgos_sys_config_get_pins_led_red();
    auto ledPin = displayPaused(clockData) ? pinRedLed : pinGreenLed;
    LOG(LL_DEBUG, ("Turning pin %d on.", ledPin));
    mgos_gpio_write(ledPin, true);
    clockData.buttonPressLedTmr = mgos_set_timer(500, 0, _buttonPressLedOffCb, &clockData);
  }

  static void processTimedActions(const uint16_t& time24h, uint8_t weekday, _clockData_t& clockData) {
    LOG(LL_DEBUG, ("processTimedActions.  Going through timed actions."));

    if (clockData.displayState == _DisplayState::MAN_PAUSED) {
      LOG(LL_DEBUG, ("processTimedActions.  Manually paused.  Ignoring all timed actions."));
      return;
    }

    for (uint8_t i = 0; i < _timedActionsCount; i++) {
      _timedAction_t& ta = clockData.actions[i];

      bool weekdayMatches = ta.weekday == _Weekday::ALL ||
                            (uint8_t) ta.weekday == weekday ||
                            ta.weekday == _Weekday::MO_FR && weekday > 0 && weekday < 6 ||
                            ta.weekday == _Weekday::SA_SU && (weekday == 0 || weekday == 6);
      if (weekdayMatches && ta.at == time24h) {
        switch (ta.action) {
          case _Action::TURN_DISPLAY_ON:
            setDisplayPaused(_DisplayState::ON, clockData);
            break;
          case _Action::TURN_DISPLAY_OFF:
            setDisplayPaused(_DisplayState::AUT_PAUSED, clockData);
            break;
          default:
            // don't do anything
            break;
        }
      }
    }
  }

  static uint16_t _wrap12hours(uint16_t time) {
    while (time >= 12 * 60) time -= 12 * 60;
    return time;
  }

  static void setEpochTime(uint32_t newEpochTime, _clockData_t& clockData) {
    LOG(LL_DEBUG, ("setEpochTime.  Retrieved time: %" PRIu32, newEpochTime));

    if (newEpochTime > _minValidEpochTime && clockData.tmpEpochTimeOffset != 0) {
      LOG(LL_DEBUG, ("Received valid time.  Clearing tmpEpochTimeOffset."));
      clockData.tmpEpochTimeOffset = 0;
    }
    uint32_t t = newEpochTime + clockData.tmpEpochTimeOffset;
    // if seconds are not valid don't update anything.
    if (t < _minValidEpochTime) return;

    clockData.epochTime = newEpochTime;
    auto timeDate = timeUtils::epochToMez(t, clockData.summertimeSwitch);
    LOG(LL_DEBUG, ("setEpochTime.  Which is %" PRIu8 ":%" PRIu8 " (summertime: %" PRIu8 ") in MEZ",
                   timeDate.tm_hour,
                   timeDate.tm_min,
                   timeDate.tm_isdst));
    uint8_t hour24 = timeDate.tm_hour;
    uint8_t minutes = timeDate.tm_min;
    uint8_t weekday = timeDate.tm_wday;
    
    auto prev = clockData.realTime;
    clockData.realTime = _wrap12hours(hour24 * 60 + minutes);

    if (prev != clockData.realTime) processTimedActions(hour24 * 60 + minutes, weekday, clockData);


    LOG(LL_DEBUG, ("setEpochTime.  Is displayTime unknown?: %" PRIu16, clockData.displayTime));
    
    if (clockData.displayTime == _unknownDisplayTime) {
      // Assume that we just rebootet and the display time is still displaying the correct value.
      // Better than always starting at 12:00.
      clockData.displayTime = clockData.realTime;
    }
  }

  static void setDisplayTime(uint16_t displayTime, _clockData_t& clockData) {
    clockData.displayTime = _wrap12hours(displayTime);
  }

  static void setTmpEpochTime(uint32_t epochTime, _clockData_t& clockData) {
    struct timeval tv;
    gettimeofday(&tv, NULL);

    if (tv.tv_sec > _minValidEpochTime || epochTime < _minValidEpochTime) {
      LOG(LL_DEBUG, ("Either time is already valid: %" PRId32 ", or new time is invalid: %" PRIu32,
                     tv.tv_sec, epochTime));
                     static_assert(sizeof(tv.tv_sec) == 4, "tv_sec");
      return;
    }

    clockData.tmpEpochTimeOffset = epochTime - tv.tv_sec;
    setEpochTime(tv.tv_sec, clockData);
  }

  static void setTimedAction(uint8_t nb, const _timedAction_t& action, _clockData_t& clockData) {
    clockData.actions[nb] = action;
    saveUserData(clockData);
  }

  // Do not modify clockData directly.  Use provided setters.
  static _clockData_t _clockData;

  // This function is called regularely and updates clockData real time values.
  // It does not set any output pins...
  static void _realTimeWatcherCb(void *arg) {
    _clockData_t& clockData = *((_clockData_t*)arg);
    
    struct timeval tv;
    gettimeofday(&tv, NULL);

    setEpochTime(tv.tv_sec, clockData);
  }

  // This function is responsible for the H-Bridge output.
  // It compares the clockData realTime with the displayTime and
  // if necessary sets the correct pin high for a short period of
  // time.
  static void _outputCb(void *arg) {
    int hBridgePins[] = { mgos_sys_config_get_pins_h_bridge_a(),
                          mgos_sys_config_get_pins_h_bridge_b() };
    _clockData_t& clockData = *((_clockData_t*)arg);

    LOG(LL_DEBUG, ("outputCb.  H-Bridge activ: %" PRIu8 " realTime: %" PRIu16", displayTime: %" PRIu16,
                   clockData.hBridgeIsActive,
                   clockData.realTime,
                   clockData.displayTime));

    auto targetTime = clockData.realTime;

    uint16_t nextCallIn = 1000; // Safe default value.

    if (clockData.hBridgeIsActive) {
      mgos_gpio_write(hBridgePins[0], false);
      mgos_gpio_write(hBridgePins[1], false);
      clockData.hBridgeIsActive = false;

      setDisplayTime(clockData.displayTime + 1, clockData);

      // Wait for 500ms before possibly incrementing again.
      nextCallIn = 500;
      goto initNextCallback;
    }

    if (displayPaused(clockData) || clockData.realTime == 0 || clockData.displayTime == _unknownDisplayTime) {
      // Wait for 1 second before trying again.
      nextCallIn = 1000;
      goto initNextCallback;
    }

    // Allow the display time to be 3 minutes in the future.
    if (clockData.displayTime < targetTime ||
        clockData.displayTime > targetTime + 3) {
      bool invertHBridge = mgos_sys_config_get_settings_invert_h_bridge();
      auto nextHBridgeOutputPin = hBridgePins[(clockData.displayTime + (invertHBridge ? 1 : 0)) & 1];
      LOG(LL_DEBUG, ("outputCb.  H-Bridge turning on %" PRIu8, nextHBridgeOutputPin));
      mgos_gpio_write(nextHBridgeOutputPin, true);
      clockData.hBridgeIsActive = true;
      nextCallIn = 300;  // Turn on for 300ms.
      goto initNextCallback;
    }

    // We had nothing to do.
    // Try again in 1 second:
    nextCallIn = 1000;
    goto initNextCallback;
  initNextCallback:
    clockData.outputCbTimerId = mgos_set_timer(nextCallIn, 0, _outputCb, arg);
  }

  static void _pauseButtonCb(int pin, void *arg) {
    _clockData_t& clockData = *((_clockData_t*)arg);
    setDisplayPaused(displayPaused(clockData) ? _DisplayState::ON : _DisplayState::MAN_PAUSED, clockData);
    (void) pin;
  }

  static void _rpcSummertimeSwitch(struct mg_rpc_request_info *ri, void *cb_arg,
                                   struct mg_rpc_frame_info *fi, struct mg_str args) {
    LOG(LL_INFO, ("RPC SummertimeSwitch"));
    _clockData_t& clockData = *((_clockData_t*)cb_arg);

    char readWrite;
    uint8_t summertimeSwitch;

    if(json_scanf(args.p, args.len, ri->args_fmt, &readWrite, &summertimeSwitch) == 2 &&
       (readWrite == 'w' || readWrite == 'W')) {
      LOG(LL_INFO, ("  Read-Write: %c, summertimeSwitch %" PRIu8, readWrite, summertimeSwitch));
      if (summertimeSwitch <= 2) {
        clockData.summertimeSwitch = (enum timeUtils::SummertimeSwitch) summertimeSwitch;
        saveUserData(clockData);
      }
    }

    mg_rpc_send_responsef(ri, "%" PRIu8, clockData.summertimeSwitch);

    (void) cb_arg;
    (void) fi;
  }

  static void _rpcDisplayState(struct mg_rpc_request_info *ri, void *cb_arg,
                               struct mg_rpc_frame_info *fi, struct mg_str args) {
    LOG(LL_INFO, ("RPC DisplayState"));
    _clockData_t& clockData = *((_clockData_t*)cb_arg);

    char readWrite;
    uint8_t displayState;

    if(json_scanf(args.p, args.len, ri->args_fmt, &readWrite, &displayState) == 2 &&
       (readWrite == 'w' || readWrite == 'W')) {
      LOG(LL_INFO, ("  Read-Write: %c, displayState %" PRIu8, readWrite, displayState));
      setDisplayPaused((enum _DisplayState) displayState, clockData);
    }

    mg_rpc_send_responsef(ri, "%" PRIu8, clockData.displayState);

    (void) cb_arg;
    (void) fi;
  }

  static void _rpcDisplayTime(struct mg_rpc_request_info *ri, void *cb_arg,
                              struct mg_rpc_frame_info *fi, struct mg_str args) {
    LOG(LL_INFO, ("RPC Display Time"));
    _clockData_t& clockData = *((_clockData_t*)cb_arg);

    char readWrite;
    uint16_t displayTime;
    if(json_scanf(args.p, args.len, ri->args_fmt, &readWrite, &displayTime) == 2 &&
       (readWrite == 'w' || readWrite == 'W')) {
      LOG(LL_INFO, ("  Read-Write: %c, displayTime %" PRIu16, readWrite, displayTime));
      setDisplayTime(displayTime, clockData);
    }


    mg_rpc_send_responsef(ri, "%" PRIu16, clockData.displayTime);

    (void) cb_arg;
    (void) fi;
  }
    
  static void _rpcEpochTime(struct mg_rpc_request_info *ri, void *cb_arg,
                            struct mg_rpc_frame_info *fi, struct mg_str args) {
    LOG(LL_INFO, ("RPC EpochTime"));
    _clockData_t& clockData = *((_clockData_t*)cb_arg);

    char readWrite;
    uint32_t tmpEpochTime;
    if(json_scanf(args.p, args.len, ri->args_fmt, &readWrite, &tmpEpochTime) == 2 &&
       (readWrite == 'w' || readWrite == 'W')) {
      // TODO validate input before writing
      LOG(LL_INFO, ("  Read-Write: %c, tmpEpochTime %" PRIu32, readWrite, tmpEpochTime));
      setTmpEpochTime(tmpEpochTime, clockData);
    }

    mg_rpc_send_responsef(ri, "%" PRIu32, clockData.epochTime + clockData.tmpEpochTimeOffset);

    (void) cb_arg;
    (void) fi;
  }

  static void _rpcTimedAction(struct mg_rpc_request_info *ri, void *cb_arg,
                              struct mg_rpc_frame_info *fi, struct mg_str args) {
                                    LOG(LL_INFO, ("RPC EpochTime"));
    _clockData_t& clockData = *((_clockData_t*)cb_arg);

    char readWrite;
    _timedAction_t timedAction;
    uint8_t nb = 0;
    if(json_scanf(args.p, args.len, ri->args_fmt, &readWrite, &nb, &(timedAction.at), &(timedAction.action), &(timedAction.weekday)) == 5 &&
       (readWrite == 'w' || readWrite == 'W')) {
      if (nb < _timedActionsCount) {
        LOG(LL_INFO, ("  Read-Write: %c, nb %" PRIu8 ", at: %" PRIu16 ", action: %" PRIu8 ", weekday: %" PRIu8, readWrite, nb, timedAction.at, timedAction.action, timedAction.weekday));
        setTimedAction(nb, timedAction, clockData);
      }
    }

    if (nb >= _timedActionsCount) nb = 0;
    mg_rpc_send_responsef(ri, "{nb:%" PRIu8 ",at:%" PRIu16 ",action:%" PRIu8 ",weekday:%" PRIu8 "}",
                          nb, clockData.actions[nb].at, (uint8_t) clockData.actions[nb].action, (uint8_t) clockData.actions[nb].weekday);

    (void) cb_arg;
    (void) fi;
  }

  static enum mgos_init_result _initApi() {
    // Public calls:
    // readWrite: either 'r' or 'w'.  Pause must always be present:  0 turn pause off.  1 turn pause on.
    mg_rpc_add_handler(mgos_rpc_get_global(),
                       "Public.DisplayState",
                       "{readWrite: %c, displayState: %" SCNu8 "}",
                       _rpcDisplayState,
                       &_clockData);
  
    // readWrite: either 'r' or 'w'.  displayed Time in minutes sind 0:00.
    mg_rpc_add_handler(mgos_rpc_get_global(),
                       "Public.DisplayTime",
                       "{readWrite: %c, displayTime: %" SCNu16 "}",
                       _rpcDisplayTime,
                       &_clockData);
    
    // readWrite: either 'r' or 'w'.  offset for epochTime  (This offset is added before calculating the date).
    mg_rpc_add_handler(mgos_rpc_get_global(),
                      "Public.EpochTime",
                      "{readWrite: %c, epochTime: %" SCNu32 "}",
                      _rpcEpochTime,
                      &_clockData);

    mg_rpc_add_handler(mgos_rpc_get_global(),
                      "Public.TimedAction",
                      "{readWrite: %c, nb: %" SCNu8 ", at: %" SCNu16 ", action: %" SCNu8 ", weekday: %" SCNu8 "}",
                      _rpcTimedAction,
                      &_clockData);

    // readWrite: either 'r' or 'w'.  summertimeSwitch must always be present:  0 automatic.  1 always summertime.  2 always wintertime
    mg_rpc_add_handler(mgos_rpc_get_global(),
                       "Public.SummertimeSwitch",
                       "{readWrite: %c, summertimeSwitch: %" SCNu8 "}",
                       _rpcSummertimeSwitch,
                       &_clockData);
    return MGOS_INIT_OK;
  }

  static enum mgos_init_result slaveClock_init(bool doFactoryReset) {
    {
      // initialize _clockData.
      ClockData tmp;
      _clockData = tmp;
    }
    LOG(LL_INFO, ("Init nebenuhr realTime: %" PRIu16 ", displayTime: %" PRIu16, _clockData.realTime, _clockData.displayTime));

    if (doFactoryReset) {
      removeUserData();
      saveUserData(_clockData);
    }

    readUserData(_clockData);

    // Initialize pins.
    mgos_gpio_set_mode(mgos_sys_config_get_pins_h_bridge_a(), MGOS_GPIO_MODE_OUTPUT);
    mgos_gpio_set_mode(mgos_sys_config_get_pins_h_bridge_b(), MGOS_GPIO_MODE_OUTPUT);
    mgos_gpio_set_mode(mgos_sys_config_get_pins_button(), MGOS_GPIO_MODE_INPUT);
    mgos_gpio_set_mode(mgos_sys_config_get_pins_led_green(), MGOS_GPIO_MODE_OUTPUT);
    mgos_gpio_set_mode(mgos_sys_config_get_pins_led_red(), MGOS_GPIO_MODE_OUTPUT);
  
    // Register a timer for watching the real time.
    mgos_set_timer(200, MGOS_TIMER_REPEAT, _realTimeWatcherCb, &_clockData);

    // Register a timer for settings h-bridge pins.
    _clockData.outputCbTimerId = mgos_set_timer(5000, 0, _outputCb, &_clockData);

    // Register a callback for button presses.
    mgos_gpio_set_button_handler(mgos_sys_config_get_pins_button(),
                                 MGOS_GPIO_PULL_UP,
                                 MGOS_GPIO_INT_EDGE_ANY,
                                 20,
                                 _pauseButtonCb,
                                 &_clockData);

    _initApi();
    return MGOS_INIT_OK;
  }
}
