# nebenuhr

An absolute bare-bones web app.

Created from templates made available by Stagehand under a BSD-style
[license](https://github.com/dart-lang/stagehand/blob/master/LICENSE).

## Build and copy to mongoose-os filesystem:

`pub build`
followed by
`cp build/web/{index.html,main.dart.js,favicon.ico} ../../fs/`